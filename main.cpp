/*
 * Computer Architecture Project 2
 * MIPS code optimizer that translates the input code to expose instruction level parallelism
 *
 * Team Members: 
 * 1. Ayush Agrawal
 * 2. Immanuel Rajkumar Philip Karunakaran
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <map>
#include <utility>
#include <stack>

//#define FILENAME "/Users/IRK/SourceCode/ILP/ILP/ILP/test.mips"
#define FILENAME "/Users/IRK/SourceCode/ILP/ILP/ILP/loops.mips"

using namespace std;

enum CmdType { LD , SD, ADD, SUB, MULT, DIV, BNEZ };
enum Location { REG, MEM, VAL }; //Operand stored in registers or memory

/* Class representing each operand in the MIPS command */
class Operand {
public:
    string name;
    Location loc;
    int offset;
    
    /* Param constrcutor */
    Operand( string s, Location l, int o): name(s), loc(l), offset(o) {}
    /* No param constructor */
    Operand(){}
};

/* Class representing each MIPS command */
class Command {
public:
    CmdType type;
    Operand op1;
    Operand op2;
    Operand target;
};

/* Class containg utility functions for strings */
class Utils {
public:
    static string & toUpStr(string & s);
    static string & ltrim(string & s);
};

/* Optimizer which operates on a MIPS program */
class Optimizer {
    vector<Command> commands;
    map <string, pair<int,int> > loops;
    stack < pair<string,int> > lpStk;
    int freePoolStart = 20, freePoolEnd = 30;
    int LOC;
    
public:
    /* Read the source code file and create a list of command objects */
    int loadSourceCode ( string filename ) {
        cout<<"\nLoading source code from : "<<filename<<endl;
        ifstream ifs(filename);
        if( !ifs ){
            cout<<"\nSource file not found"<<endl;
            return -1;
        }
        string line; int lno=1;
        while(getline(ifs,line)){
            line = Utils::toUpStr(line);
            commands.push_back(parseCmd(line, lno));
            lno ++;
        }
        LOC = (int)commands.size();
        return 0;
    }
    
    /* Parse each line of the program to form a command object */
    Command parseCmd ( string line , int lineno ) {
        Command c;
        string token;
        
        stringstream ss(line);
        Utils::toUpStr(line);
        
        int pos = (int)line.find(":");
        if( pos != string::npos) {
            string loopName;
            ss >> loopName;
            loopName = loopName.substr(0, loopName.size()-1);
            //cout<<"\nLoopName: -"<<loopName<<"-";
            lpStk.push(make_pair(loopName, lineno));
        }
        
        ss >> token;
        if( token == "BNEZ") {
            c.type = BNEZ;
            auto t = lpStk.top();
            lpStk.pop();
            string opndname, loopName;
            ss >> opndname >> loopName;
            if( t.first != loopName){
                cout<<"\n Not matching loopname"<<endl;
            } else {
                loops[loopName] = make_pair(t.second, lineno);
            }
            Operand op(opndname, REG, 0);
            c.target = op;
            return c;
        }
        else if( token == "LD") c.type = LD;
        else if( token == "SD") c.type = SD;
        else if( token == "ADD") c.type = ADD;
        else if( token == "SUB") c.type = SUB;
        else if( token == "MUL") c.type = MULT;
        else if( token == "DIV") c.type = DIV;
                else {
            cout<<"\nInvalid command"<<lineno<<endl;
        }
        
        if( c.type == LD || c.type == SD ) {
            string opndname;
            ss >> opndname;
            Operand t(opndname, REG, 0);
            string opStr; // This would generally have format offset(source reg). Ex LW r2 4(r1)
            ss >> opStr;
            cout<<"\nLDSD CMD - "<<opndname<<" "<<opStr;
            int spos = (int)opStr.find("(");
            int epos = (int)opStr.find(")");
            if( spos != string::npos && epos != string::npos) {
                string offset = "", srcreg = "";
                offset = opStr.substr(spos+1, (int)(epos-spos-1));
                srcreg = opStr.substr(epos+1, opStr.size()-epos-1);
                //cout<<"\nOffset: -"<<offset<<"-  source reg -"<<srcreg<<"-";
                int offsetInt = stoi(offset);
                Operand op1(srcreg, REG, offsetInt );
                c.target = t;
                c.op1 = op1;
            }
            
        } else if ( c.type == ADD || c.type == SUB || c.type == MULT || c.type == DIV ) {
            string opndname;
            ss >> opndname;
            Operand t(opndname, REG, 0);
            /* Need to check if the operands are registers or direct values -  Assuming it is regs now*/
            ss >> opndname;
            Operand op1(opndname, REG, 0);
            ss >> opndname;
            Operand op2(opndname, REG, 0);
            c.target = t;
            c.op1 = op1;
            c.op2 = op2;
        }
        
        return c;
    }
    
    /* Routine to do unrolling of existing loops in programs */
    void optimizeLoops ( ) {
        cout<<"\nLoop Unrolling";
        auto MBEGIN = loops.begin(), MEND = loops.end();
        for( auto itr = MBEGIN; itr != MEND; ++itr ){
            cout<<"\n\tProcessing Loop:"<<itr->first;
            cout<<" between lines "<<itr->second.first<<":"<<itr->second.second;
            
            vector<Command> loopBody;
            Operand loopItr = commands[itr->second.second-1].target;
            cout<<"\nLoop Iterator in this loop : "<<loopItr.name;
            
            for( int k=itr->second.first-1; k <itr->second.second; k++ ) {
                if( commands[k].op1.name == loopItr.name && commands[k].type == SD ) {
                    //Add new loop body 4 time.
                    int changingOffset = commands[k].op1.offset;
                    for( int h=0; h<4; h++ ) {
                        Command temp;
                        temp.type = commands[k].type;
                        temp.target = commands[k].target;
                        temp.op1 = commands[k].op1;
                        temp.op1.offset = changingOffset;
                        changingOffset += -8;
                        loopBody.push_back(temp);
                    }
                } else if( commands[k].op1.name == loopItr.name && commands[k].type == LD ) {
                    //Add new loop body 4 time.
                    int changingOffset = commands[k].op1.offset;
                    for( int h=0; h<4; h++ ) {
                        Command temp;
                        temp.type = commands[k].type;
                        temp.target = commands[k].target;
                        temp.op1 = commands[k].op1;
                        temp.op1.offset = changingOffset;
                        changingOffset += -8;
                        loopBody.push_back(temp);
                    }
                } else if (commands[k].type != BNEZ ) {
                    for( int h=0; h<4; h++ ) {
                        Command temp;
                        temp.type = commands[k].type;
                        temp.target = commands[k].target;
                        temp.op1 = commands[k].op1;
                        //Change the target register name
                        temp.op2 = commands[k].op2;
                        loopBody.push_back(temp);
                    }
                }
            }
            for( auto ltr = loopBody.begin(); ltr != loopBody.end(); ++ltr )
                cout<<"\n"<<formCmdString(*ltr);
        }
        cout<<"\nLoop Unrolling done"<<endl;
    }
    
    /* Routine to generate a command string given an object */
    /*  { LD , SD, ADD, SUB, MULT, DIV, BNEZ } */
    string formCmdString ( Command c ){
        string s;
        stringstream ss;
        switch (c.type) {
            case LD : {
                ss << "LD "<<c.target.name<<"  ("<<c.op1.offset<<")"<<c.op1.name;
                break;
            }
            case SD : {
                ss << "SD "<<c.target.name<<"  ("<<c.op1.offset<<")"<<c.op1.name;
                break;
            }
            case ADD : {
                ss << "ADD "<<c.target.name<<" "<<c.op1.name<<" "<<c.op2.name;
                break;
            }
            case SUB : {
                ss << "SUB "<<c.target.name<<" "<<c.op1.name<<" "<<c.op2.name;
                break;
            }
            case MULT : {
                ss << "MULT "<<c.target.name<<" "<<c.op1.name<<" "<<c.op2.name;
                break;
            }
            case DIV : {
                ss << "DIV "<<c.target.name<<" "<<c.op1.name<<" "<<c.op2.name;
                break;
            }
            case BNEZ : {
                ss << "BNEZ "<<c.target.name;
                break;
            }
        }
        s = ss.str();
        return s;
    }
    
    /* Routine to do data dependence removal */
    void removeDataDependence ( ) {
        cout<<"\nRemoving Data Dependence";
        cout<<"\n\nListing all commands in the program :";
        /* Loop to iterate through the list of commands. In case of a n2 algorithm we may need to have a similar nested loop */
        
         auto CBEGIN = commands.begin(), CEND = commands.end();
         for( auto itr = CBEGIN; itr != CEND; ++itr ){
            cout<<"\n\tCommand : Type="<< itr->type <<" Target="<<itr->target.name;
        }
        
        cout<<"\nRemoving Data Dependence done"<<endl;
    }
    
    /* Routine to interpret branch prediction */
    /* 
     * Since static branch prediction, we may need to record the outcome of various runs
     * in some file that we can use in the later stages.
     */
    void branchPredictionStatic ( ) {
        /* Need to store the outcome of the various runs in some file. */
    }
};

/* Program control */
int main(int argc, const char * argv[]) {
    Optimizer optimizer;
    optimizer.loadSourceCode(FILENAME);
    optimizer.optimizeLoops();
    //optimizer.removeDataDependence();
    
    return 0;
}

/* String utility functions */
string & Utils::toUpStr( string & s ) {
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c) { return std::toupper(c); });
    return s;
}

string & Utils::ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}
